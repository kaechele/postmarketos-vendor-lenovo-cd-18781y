# Firmware Files for Lenovo ThinkSmart View (CD-18718Y)

| File in Repository                      | Path on Stock Device                                | Path for Mainline                                      |
| --------------------------------------- | --------------------------------------------------- | ------------------------------------------------------ |
| bt/rampatch_00150100.bin                | /modem/image/btfwnpla.tlv                           | /lib/firmware/qca/rampatch_00150100.bin                |
| bt/nvm_00150100.bin                     | /modem/image/btnvnpla.bin                           | /lib/firmware/qca/nvm_00150100.bin                     |
| gpu/a506_zap.b02                        | /vendor/firmware/a506_zap.b02                       | /lib/firmware/postmarketos/a506_zap.b02                |
| gpu/a506_zap.mdt                        | /vendor/firmware/a506_zap.mdt                       | /lib/firmware/postmarketos/a506_zap.mdt                |
| sound/tas5728m_dsp_lenovo_cd-18781y.bin | part of kernel driver (tas5872m.h)                  | /lib/firmware/tas5728m_dsp_lenovo_cd-18781y.bin        |
| wifi/board.bin                          | /modem/image/bdwlan30.bin                           | /lib/firmware/ath10k/QCA9379/hw1.0/board.bin           |
| wifi/firmware-sdio-6.bin                | /modem/image/otp30.bin<br> /modem/image/qwlan30.bin | /lib/firmware/ath10k/QCA9379/hw1.0/firmware-sdio-6.bin |

## Firmware Versions

| Firmware | Version                            |
| -------- | ---------------------------------- |
| bt       | BTFM.NPL.1.0.4-00002-QCABTFMSWPZ-1 |
| wifi     | WLAN.NPL.1.6-00163-QCANPLSWPZ-1    |

## Building the WiFi Firmware

1. Clone https://github.com/qca/qca-swiss-army-knife
2. Get `/modem/image/otp30.bin` and `/modem/image/qwlan30.bin` from stock image
   or device
3. From `qca-swiss-army-knife/tools/scripts/ath10k` use `ath10k-fwencoder`. As
   the `firmware-version` use the version string of the WLAN package from the
   BSP that was used to create the firmware. This info can be found on the
   device in the file `/modem/verinfo/ver_info.txt`.
   ```bash
   ath10k-fwencoder --create \
       --firmware-version="WLAN.NPL.1.6-00163-QCANPLSWPZ-1" \
       --otp=otp30.bin \
       --firmware=qwlan30.bin \
       --set-wmi-op-version=tlv \
       --set-htt-op-version=tlv \
       --set-fw-api=6 \
       --features=wowlan,ignore-otp-result,mfp-support \
       --output=firmware-sdio-6.bin
   ```
4. This should result in a `firmware-sdio-6.bin` output file which can put into
   the according firmware directory on the target.

## Checksums (SHA256)

```
4e36326d644ada724b5bb3358e5b8d867fece729f645caa46bf9f051db06c19e  bt/nvm_00150100.bin
0fa3169e0db081fa412243bb94cbbc0c7a297a924b478903690df95c430e564e  bt/rampatch_00150100.bin
fe8dead286927c662da0769589c739f914c130271429701606bd7662e91930fd  gpu/a506_zap.b02
5a73540609c167ef5c208043c4f1fa045a7c652c3994c5c87647ee0ca3154131  gpu/a506_zap.mdt
f78beb1c4f10d2e43695a0f7c804acc93b8826fc474739dfed36e14df8e204a8  sound/tas5728m_dsp_lenovo_cd-18781y.bin
7b738dc57ced668259927390daa205b51e10872839e3c5e8df2a227911d13c2d  wifi/board-2.bin
5a1e20e58c02c53790735f0387b2696005677fcb34d9e51f7482333e4403f2ef  wifi/firmware-sdio-6.bin
```

## WiFi firmware checksums (SHA256)

### `WLAN.NPL.1.6-00163-QCANPLSWPZ-1`

```
c6d4e1eb2891ea4ae009622b9d7178397413fb6be6464549f1fa64e8ad9e3066  otp30.bin
2914b3a5ca20705ff1c94ce70c88dad79f29f4a752af402c03483a837f322ceb  qwlan30.bin
```
